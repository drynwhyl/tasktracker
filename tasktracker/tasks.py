# -*- coding: UTF-8 -*-
import base64
import pickle
import os


class Task:

    def __init__(self, name, description="", priority=0):
        self.name = name
        self.description = description
        self.priority = priority
        self.subtasks = []
        self.status = 0
        self.path = None


    @property
    def status(self):
        if not self.subtasks:
            return self._status
        else:
            return sum([st.status for st in self.subtasks]) / len(self.subtasks)

    @status.setter
    def status(self, new_value):
        if not self.subtasks:
            self._status = new_value
        else:
            for st in self.subtasks:
                st.status = new_value


    def get_status_string(self):
        return "{:.0f}%".format(self.status * 100)


    def __str__(self):
        return """{}
    Status: {}
    Priority: {}
    {}""".format(self.name, self.status, self.priority, self.description)

    def full_string(self, tab_count=0, print_status=False):
        from io import StringIO
        file_str = StringIO()

        for _ in range(tab_count):
            print("    ", end="", file=file_str)

        print(self.name, file=file_str)

        for st in self.subtasks:
            print(st.full_string(tab_count + 1, print_status), end="", file=file_str)

        return file_str.getvalue()




class TaskStorage:

    
    def __init__(self, store_path=os.path.dirname(os.path.abspath(__file__)) + "/data/"):
        self.store_path = store_path

        if not os.path.exists(self.store_path):
            os.makedirs(self.store_path)


    def get_file_path(self, task_name):
        return self.store_path + task_name


    def store_task(self, task, file_name):
        file_path = self.get_file_path(file_name)
        with open(file_path, "wb+") as output_file:
            pickle.dump(task, output_file)


    def delete_task(self, file_name):
        file_path = self.get_file_path(file_name)
        os.remove(file_path)


    def load_task(self, file_name):
        file_path = self.get_file_path(file_name)
        if os.path.exists(file_path):
            with open(file_path, "rb+") as input_file:
                return pickle.load(input_file)
        else:
            return None
    

    def filelist(self):
        for dirpath,_,filenames in os.walk(self.store_path):
            for f in filenames:
                yield os.path.abspath(os.path.join(dirpath, f))


class TaskPath:


    def __init__(self, string_path):
        self.path_elements = [string_path.split(".")]


    def __str__(self):
        return ".".join(self.path_elements)

    
    def __eq__(self, value):
        return  str(self) == str(value)


    @property
    def levels(self):
        return len(self.path_elements)

    @property
    def name(self):
        return self.path_elements[-1]
    



class TaskIndex:
    
    def __init__(self, filename):
        self.dictionary = {}



class IdGenerator:


    def __init__(self):
        self.recycle_next = []
        self.values = []
        self.recycle = 0
        self.count = 0


    def generate(self):
         
        if self.recycle is None:
            self.count += 1
            id = self.count
        else:
            id = self.recycle
            self.recycle = self.recycle_next[self.recycle]

        return id


    def free(self, id):
        self.recycle_next[id] = self.recycle
        self.recycle = id



from anytree import Node, RenderTree

class TaskController:


    def __init__(self):
        self.id_generator = IdGenerator()
        self.task_storage = TaskStorage()
        self.task_index = TaskIndex(self.task_storage.store_path + ".dict")


    def get_task_filename(self, id):
        return str(id)


    def add_task(self, task_path):
        id = self.id_generator.generate()
        self.task_index.dictionary[task_path] = id

        task = Task(task_path.name)
        task.path = task_path
        self.task_storage.store_task(task, self.get_task_filename(id))


    def remove_task(self, task_path):
        id = self.task_index.dictionary[task_path]
        if id is not None:
            self.id_generator.free(id)
            self.task_index.dictionary[task_path] = None
            self.task_storage.delete_task(id)


    def edit_task(self, task_path, field, value):
        task = self.task_storage.load_task(task_path)
        task.__setattr__(field, value)


    def print_tasks(self):

        def _add2tree(task, parent):
            new_parent = Node(task.name, parent)

            for st in task.subtasks:
                _add2tree(st, new_parent)

        filelist = self.task_storage.filelist()

        if filelist:

            root = Node("tasks")

            for file in filelist:
                with open(file, "rb+") as input_file:
                    task = pickle.load(input_file)

                _add2tree(task, root)

            for pre, _, node in RenderTree(root):
                print("%s%s" % (pre, node.name))

        else:
            print("There is not tasks added yet")

    
    def save_state(self):
        with open(".controller_data", "wb+") as output_file:
            pickle.dump(self, output_file)