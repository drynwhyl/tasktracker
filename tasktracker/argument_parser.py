# -*- coding: UTF-8 -*-

import sys
import argparse

def create_parser():
    parser = argparse.ArgumentParser(
        prog="tasktracker",
        description="Simple task tracker, can handle single and nested tasks",
        epilog=
            "Simple program that allows you "
            "to track your progress in set of "
            "different tasks and visualise it.",
        add_help=False
    ) 


    parent_group = parser.add_argument_group(title="Arguments")

    parent_group.add_argument('--help', '-h', action='help', help='Display this help message')

    subparsers = parser.add_subparsers(
        dest="command",
        title="Availale commands",
        description="Actions that can be performed with tasks"
    )

    add_parser = subparsers.add_parser("add", 
        add_help=False,
        help="Add new task",
        description="Adds new task to the list."
    )
    add_group = add_parser.add_argument_group(title="Options")
    add_group.add_argument(
        "task_name",
        help="Name of new task", 
        metavar="NAME"
    )
    add_group.add_argument('--help', '-h', action='help', help='Help')

    remove_parser = subparsers.add_parser("remove", 
        add_help=False,
        help="Remove existing task",
        description="Finds task by name and removes it"
    )
    remove_group = remove_parser.add_argument_group(title="Options")
    remove_group.add_argument(
        "task_name",
        help="Name of the task to be removed. Nested task concats using dot: parenttask.childtask.",
        metavar="NAME"
    )
    remove_group.add_argument('--help', '-h', action='help', help='Help')
 
    view_parser = subparsers.add_parser("view",
        add_help=False,
        help="View existing tasks",
        description="Prints a tree-like scheme of task hierarchy"
    )
    view_group = view_parser.add_argument_group(title="View options")
    view_group.add_argument(
        "--order",
        "-o",
        help="Display order",
        metavar="ORDER"
    )
    view_group.add_argument(
        "--field",
        "-f",
        help="Field by which tasks is sorted",
        metavar="FIELD"
    )

    return parser
